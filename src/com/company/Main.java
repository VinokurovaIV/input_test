package com.company;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {

        ArrayList<String> list = new ArrayList<>();

        list.add("Иванов");
        list.add("Петров");
        list.add("Сидоров");

        String report = generateReport(list);
        saveToFile(report, "report.html");

    }

    private static String generateReport(ArrayList<String> list) {
        StringBuilder stringBuilder = new StringBuilder()
                .append("<!DOCTYPE html>\n")
                .append("<html lang=\"en\">\n")
                .append("<head>\n")
                .append("    <meta charset=\"UTF-8\">\n")
                .append("    <link rel=\"stylesheet\" type=\"text/css\" href=\"stylesheet.css\" title=\"Style\">\n")
                .append("    <title>TEST</title>\n")
                .append("<style>\n")
                .append("        body {\n")
                .append("            background-color: #fff;\n")
                .append("            color: #353833;\n")
                .append("            font-family: Arial, sains-serif;\n")
                .append("            font-size: 14px;\n")
                .append("        }\n")
                .append("\n")
                .append("    </style>")
                .append("</head>\n")
                .append("<body>\n")
                .append("    <h1>TEST</h1>\n");

        for (int i = 0; i < list.size(); i++) {
            stringBuilder.append("   <div>")
            .append(list.get(i))
            .append("</div>\n");
        }

        stringBuilder
                .append("</body>\n")
                .append("</html>");

        return stringBuilder.toString();

        }

    private static void saveToFile(String report, String fileName) throws IOException {
        OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(fileName));
        writer.write(report);
        writer.close();
    }


}
